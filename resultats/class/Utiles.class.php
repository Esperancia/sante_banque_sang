<?php 
class Utiles{
	public function ListeDepartements()
	{
		require('connexbd.php');	
		$req='select * from departements ORDER BY Departement ASC'; 	
				
		// ex�cuter la req
		$result = $bdd->query($req);
		// R�cup�ration de toutes les lignes du jeu de r�sultats
		$recup=$result->fetchAll();
		//fermer la requete
		$result->closeCursor();
		//returner le tableau des elements req
		return $recup;
	}

	public function GroupesSanguins()
	{
		require('connexbd.php');	
		$req='select GroupeS from compatibilite'; 	
				
		// ex�cuter la req
		$result = $bdd->query($req);
		// R�cup�ration de toutes les lignes du jeu de r�sultats
		$recup=$result->fetchAll();
		//fermer la requete
		$result->closeCursor();
		//returner le tableau des elements req
		return $recup;
	}
	
	public function ListeBSDepartement($Departem)
	{
		require('connexbd.php');	
		$req='select * from structure where Departement=?'; 	
				
		// ex�cuter la req
		$result = $bdd->prepare($req);	
		$result->execute(array($Departem));
		// R�cup�ration de toutes les lignes du jeu de r�sultats
		$recup=$result->fetchAll();
		//fermer la requete
		$result->closeCursor();		
		//returner le tableau des elements req
		return $recup;
	}
	
	public function Compatibilite($GroupeS)
	{
		require('connexbd.php');	
		$req='select EnRemplacem from compatibilite where GroupeS=?'; 	
				
		// ex�cuter la req
		$result = $bdd->prepare($req);	
		$result->execute(array($GroupeS));
		// R�cup�ration de toutes les lignes du jeu de r�sultats
		$recup=$result->fetchAll();
		//fermer la requete
		$result->closeCursor();		
		//returner le tableau des elements req
		return $recup;
	}
	
	public function tabCompa($ListeCompa)
	{	
		$tabGroupesCompa=explode(";",$ListeCompa[0]);
		return $tabGroupesCompa;
	}
	
	public function BSEtStockDmd($listeBS, $tabGroupesCompa)
	{
		$tout[]='';
		require('connexbd.php');	
		foreach($listeBS as $Structure)
		{
			 foreach($tabGroupesCompa as $chakGS){
                   $req= "select structure.Nom, groupessanguins.GroupeS,groupessanguins.Qte 
							from groupessanguins, structure 
							where structure.IdStructure=groupessanguins.IdStructure and groupessanguins.GroupeS=? 
							and structure.IdStructure=?";
					// ex�cuter la req
					$result = $bdd->prepare($req);	
					$result->execute(array($chakGS,$Structure[0]));
					// R�cup�ration de toutes les lignes du jeu de r�sultats
					$recup=$result->fetchAll();
					//fermer la requete
					$result->closeCursor();		
					//returner le tableau des elements req
					$tout[]=$recup;
              }
		}
		return $tout;
	}
	
	public function searchGS($Departem, $GroupeS)
	{
		require('connexbd.php');	
		$ListeBS=$this->ListeBSDepartement($Departem);
		$ListeCompa = $this->Compatibilite($GroupeS);
		
		$TableauListeCompa=$this->tabCompa($ListeCompa[0]);
		$rs = $this->BSEtStockDmd($ListeBS, $TableauListeCompa);
		return $rs;
	}
	
	//recuperer en json ou xml
	
	public function Enreg($tabl)
	{
		require('connexbd.php');	
		$req='select * from '. $tabl; 	
				
		// ex�cuter la req
		$rpx = $bdd->query($req);

		//create one master array of records
		$posts=array();
		//parcourir ligne par ligne le jeu d'enr obtenu
		while($lign = $rpx->fetch()){
		 $posts[]=array('post'=>$lign);
		}
		//if($format=='json'){
		 header('Content-type:application/json');
		 echo json_encode(array('posts'=>$posts));
		/*}
		
		else{
		 header('Content-type:text/xml');
		 echo '<posts>';
		 foreach($posts as $index=>$post){
		  if (is_array($post)){
		     foreach($post as $key=>$value){
			   echo '<'.$key.'>';
			 }
		  }
		 }
		 echo '</posts>';	 
		}
		*/
		
	//fermer la requete
	 //$rpx->closeCursor();
	}
}
