﻿<?php 
	include_once('class/connexbd.php');
	include_once('class/Utiles.class.php');
					
	$instanceUtiles= new Utiles();
	
	if ((isset($_POST['cmbDepart'])) && (isset($_POST['cmbGroupeS']))){
		//maintenir les selections precedemment faites
			$choixDepart= $_POST['cmbDepart'];
			$choixGroupe= $_POST['cmbGroupeS'];
						
	}else{
		$choixDepart=0;
		$choixGroupe=0;
	}
?>
<!DOCTYPE HTML>
<html>
	 <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bienvenue dans votre application des recherches de banques de sang</title>
        <link href="styles.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div class="total">
        <h3>Bienvenue dans votre application des recherches de banques de sang !</h3>
        
      	<form action="questionnerStockGsStructure.php" method="post">
            <div class="selection">
              <label for="cmbDepart"></label>
              <select name="cmbDepart" id="cmbDepart">
                <option value="0">--Choisissez un département--</option>
                <?php 		
					$listeDepart=$instanceUtiles->ListeDepartements();
					foreach($listeDepart as $Departem){
						echo '<option value="'. $Departem['Departement'].'"' ;
							if ($choixDepart==$Departem['Departement']) echo ' selected>';
							else {echo ' >';}
						echo $Departem['Departement'];
						echo '</option>';
					}
				?>
                </select>
            </div>
			<br>
          <div class="selection">
            <label for="cmbGroupeS"></label>
              <select name="cmbGroupeS" id="cmbGroupeS">
                <option value="0">--Choisissez un groupe sanguin--</option>
                <?php 
					$listeGroupesS=$instanceUtiles->GroupesSanguins();
					foreach($listeGroupesS as $Groupe){
						echo '<option value="'. $Groupe['GroupeS'].'" ';
							if ($choixGroupe==$Groupe['GroupeS']) {
								echo ' selected>';
								}
							else {echo ' >';}
						echo utf8_encode($Groupe['GroupeS']). '</option>';
					}
				?>
            </select>
          </div>
			<br>
          <input name="BtnStart" class="start" type="submit" value="RECHERCHER" />
        </form>

		
		
        <div class="resultats">
           	<?php 
			/*
					$testsplit=explode(";","A+;A-;O+;AB+");
					echo $testsplit;
			*/		
					
				if ((isset($_POST['cmbDepart'])) && (isset($_POST['cmbGroupeS']))){
					//echo $_POST['cmbDepart']; echo $_POST['cmbGroupeS'];
					
					if (($_POST['cmbDepart']=="0") || ($_POST['cmbGroupeS']=="0")){
						echo 'Faites un choix correct du département et du groupe sanguin!';
					}	
					else{
						
						//ici commence les manip
						$ListeBS=$instanceUtiles->ListeBSDepartement($_POST['cmbDepart']);
						$ListeCompa = $instanceUtiles->Compatibilite($_POST['cmbGroupeS']);
						
						$TableauListeCompa=$instanceUtiles->tabCompa($ListeCompa[0]);
						echo '<div class="center"><table>';		
									$r='';
						foreach($ListeBS as $Structure)
						{
							 foreach($TableauListeCompa as $chakGS){
								   $req= "select structure.Nom, groupessanguins.GroupeS,groupessanguins.Qte 
											from groupessanguins, structure 
											where structure.IdStructure=groupessanguins.IdStructure and groupessanguins.GroupeS=? 
											and structure.IdStructure=?";
									// exécuter la req
									$result = $bdd->prepare($req);	
									$result->execute(array($chakGS,$Structure[0]));
									// Récupération de toutes les lignes du jeu de résultats
									$recup=$result->fetchAll();
									//fermer la requete
									$result->closeCursor();		
									//returner le tableau des elements req
									$recup;
									//traiter le result

									foreach($recup as $elm){
										if($recup[0]['Nom']==$r)
										{
										echo '<tr border="1" class="sugg"><td>'.$recup[0]["GroupeS"].'</td>';
										echo '<td>'.$recup[0]["Qte"].'</td></tr>';
										}
										else
										{
										$r=$recup[0]['Nom'];	
										echo '<tr><td colspan="2" class="structure">'.utf8_encode($recup[0]["Nom"]).'</td></tr>';
										echo '<tr class="important"><td>'.$recup[0]["GroupeS"].'</td>';
										echo '<td>'.$recup[0]["Qte"].'</td></tr>';
										echo '<tr><td colspan="2" class="suggestion">Autres groupes sanguins compatibles suggérés: </td></tr>';
										}
								
									}
									
							  }
							
						}
		
						
						/*
						//traitement ligne par ligne et formatage du résultat
						$res=$instanceUtiles->searchGS($_POST['cmbDepart'], $_POST['cmbGroupeS']);
						echo $res;
						$r='';
						
						echo '<table>';
						foreach($res as $elm){
							if($res[0]['Nom']==$r)
							{
							echo '<tr><td>'.$res[0]["GroupeS"].'</td>';
							echo '<td>'.$res[0]["Qte"].'</td></tr>';
							}
							else
							{
							$r=$res[0]['Nom'];	
							echo '<tr><td colspan="2">'.utf8_encode($res[0]["Nom"]).'</td></tr>';
							echo '<tr><td>'.$res[0]["GroupeS"].'</td>';
							echo '<td>'.$res[0]["Qte"].'</td></tr>';
							}
						
						}
						echo '</table>';
						*/
						
						echo '</table></div>'; 
					}
				
				}	
			?> 
        </div>

      </div>

 
    </body> 
</html>

